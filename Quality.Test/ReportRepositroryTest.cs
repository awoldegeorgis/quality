﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using Quality.Web.Controllers;
using Quality.Web.Provider;
using Quality.Web.Repository;

namespace Quality.Test
{
    [TestFixture]
    public class ReportRepositroryTest
    {

        public IReportRepository ReportRepository { get; private set; }

        public IProvider mockProvider { get; private set; }

        [SetUp]
        public void Setup()
        {
            var mockProvider = new Mock<IProvider>();
            mockProvider.Setup(x => x.GetReportData()).Returns(MockItemsCreater.GetDataSet);

            ReportRepository = new ReportRepository(mockProvider.Object);
        }


        [Test]
        public  void TestTotoalItemCount()
        {
            var page = ReportRepository.GetReport(null);
            Assert.IsNotNull(page);
            Assert.Equals(10, page.Count());
        }

    }
}
