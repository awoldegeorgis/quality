﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quality.Web.Models;

namespace Quality.Test
{
    public class MockItemsCreater
    {
        public static IQueryable<QualifiedPartsList> GetMockReport()
        {
            var mockList = new List<QualifiedPartsList>();

            for (int i = 0; i < 100; i++)
            {
                var item = new QualifiedPartsList();
                item.PartNumber = i.ToString();
                mockList.Add(item);
            }

            return mockList.AsQueryable();
        }

        public static DataSet GetDataSet()
        {
            //Create a test DataSet
            DataSet set = new DataSet();

            //Create a test dataTable
            DataTable table = new DataTable();
            table.Columns.Add("PartNumber");
            table.Columns.Add("Revision");
            table.Columns.Add("PartName");
            table.Columns.Add("ToolDieSetNumber") ;
            table.Columns.Add("QPL", typeof(Boolean)) ;
            table.Columns.Add("OpenPO", typeof(Boolean)) ;
            table.Columns.Add("Part Juisdication") ;
            table.Columns.Add("Part Classification") ;
            table.Columns.Add("Supplier Company Name") ;
            table.Columns.Add("Supplier Company Code") ;
            table.Columns.Add("CTQ", typeof(Boolean)) ;
            table.Columns.Add("UserName") ;
            table.Columns.Add("LastUpdatedDateUtc", typeof(DateTime));

            //Create test dataItem 
            for (var i = 0; i < 10; i++)
            {               
                table.Rows.Add("PartNumber"+ i,null,"PartName"+ i,null,1,1,null,null,"Supplier Company Name"+i,"Supplier Company Code"+i,1,
                    "UserName"+i,new DateTime());                    
            }
            set.Tables.Add(table);
            return set;
        }
    }
}
