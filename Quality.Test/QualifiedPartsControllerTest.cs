﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Moq;
using NUnit.Framework;
using Quality.Web.Controllers;
using Quality.Web.Models;
using Quality.Web.Repository;

namespace Quality.Test
{
    [TestFixture]
    public class QualifiedPartsControllerTest
    {

        public QualifiedPartsController Controller { get; private set; }

        public IReportRepository repository { get; private set; }

        [SetUp]
        public void Setup()
        {
            var mockRepositroy = new Mock<IReportRepository>();
            mockRepositroy.Setup(x => x.GetReport(It.IsAny<string>())).Returns(MockItemsCreater.GetMockReport());
            Controller = new QualifiedPartsController(mockRepositroy.Object);

        }
        
        [Test]
        public async Task TestTotalPageCount()
        {
            var page = await GetPagedEmployeeResult(1, 15);
            Assert.That(page.Content.TotalNumberOfPages, Is.EqualTo(7));
        }

        [Test]
        public async Task TestTotalPageCountWithModOfZero()
        {
            var page = await GetPagedEmployeeResult(3, 10);
            Assert.That(page.Content.TotalNumberOfPages, Is.EqualTo(10));
        }

        [Test]
        public async Task TestPageSize()
        {
            var page = await GetPagedEmployeeResult(1, 15);
            Assert.That(page.Content.PageSize, Is.EqualTo(15));
        }

        [Test]
        public async Task TestTotalNumberOfRecords()
        {
            var page = await GetPagedEmployeeResult(1, 15);
            Assert.That(page.Content.TotalNumberOfRecords, Is.EqualTo(100));
        }

        [Test]
        public async Task TestPageNumber()
        {
            var page = await GetPagedEmployeeResult(3, 15);
            Assert.That(page.Content.PageNumber, Is.EqualTo(3));
        }

        private async Task<OkNegotiatedContentResult<PagedResults<QualifiedPartsList>>> GetPagedEmployeeResult(int page, int pageSize = 10, string orderBy = nameof(QualifiedPartsList.PartNumber), bool ascending = true)
        {
            var get = await Controller.Get(page, pageSize, orderBy, ascending);
            Assert.IsInstanceOf<OkNegotiatedContentResult<PagedResults<QualifiedPartsList>>>(get);

            return (OkNegotiatedContentResult<PagedResults<QualifiedPartsList>>)get;
        }

    }
}
