﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Services;
using log4net;
using Quality.Web.Models;
using Quality.Web.Repository;

namespace Quality.Web.Controllers
{

    public class QualifiedPartsController : ApiController
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IReportRepository _reportRepository;

        public QualifiedPartsController(IReportRepository reportRepository)
        {
            _reportRepository = reportRepository;
        }

        #region Qualified Parts APIs

        /// <summary>
        /// Get report as paginated and page size
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="ascending"></param>
        /// <param name="searchBy"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get(
            int? page = null, 
            int pageSize = 10, 
            string orderBy = nameof(QualifiedPartsList.PartNumber), 
            bool ascending = true,
            string searchBy = null)
        {
            try
            {
                if (page == null)
                    return Ok(_reportRepository.GetReport(null));


                var qualifiedList = await CreatePagedResults<QualifiedPartsList>
                    (_reportRepository.GetReport(null).AsQueryable(), page.Value, pageSize, orderBy, ascending);

                Log.Info("Successful request");
                return Ok(qualifiedList);
            }
            catch (ArgumentException ex)
            {
                Log.Error("Bad request received from the user.", ex);
                return BadRequest();
            }
            catch (Exception ex)
            {
                Log.Error("Unhandled exception occured while processing request", ex);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryable"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="ascending"></param>
        /// <returns></returns>
        private async Task<PagedResults<T>> CreatePagedResults<T>(
            IQueryable<T> queryable,
            int page,
            int pageSize,
            string orderBy,
            bool ascending
            )
        {
            var skipAmount = pageSize * (page - 1);

            var projection = queryable
                .Skip(skipAmount)
                .Take(pageSize)
                .Cast<T>(); 

            var totalNumberOfRecords =  queryable.Count();
            var results =  projection;

            var mod = totalNumberOfRecords % pageSize;
            var totalPageCount = (totalNumberOfRecords / pageSize) + (mod == 0 ? 0 : 1);

            var nextPageUrl =
                page == totalPageCount
                    ? null
                    : Url?.Link("DefaultApi", new
                    {
                        page = page + 1,
                        pageSize,
                        orderBy,
                        ascending
                    });

            return new PagedResults<T>
            {
                Results = results,
                PageNumber = page,
                PageSize = results.Count(),
                TotalNumberOfPages = totalPageCount,
                TotalNumberOfRecords = totalNumberOfRecords,
                NextPageUrl = nextPageUrl
            };
        }

        #endregion
    }

}