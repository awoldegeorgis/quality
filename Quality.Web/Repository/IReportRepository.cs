﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quality.Web.Models;

namespace Quality.Web.Repository
{
    public interface IReportRepository
    {
        IQueryable<QualifiedPartsList> GetReport(string searchBy);
    }
}
