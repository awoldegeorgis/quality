﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Quality.Web.Models;
using System.Globalization;
using log4net;
using Quality.Web.Provider;

namespace Quality.Web.Repository
{
    public class ReportRepository : IReportRepository
    {

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IProvider _provider;

        public ReportRepository(IProvider provider)
        {
            this._provider = provider;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="searchBy"></param>
        /// <returns></returns>
        public IQueryable<QualifiedPartsList> GetReport(string searchBy)
        {
            try
            {
                var qualifiedPartsList = new List<QualifiedPartsList>();
                var dataSet = _provider.GetReportData();

                MapReportDataSourceToModel(dataSet, qualifiedPartsList, searchBy);

                return qualifiedPartsList.AsQueryable();
            }
            catch (Exception ex)
            {
                Log.Error("Exception occured while processing request", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Map database response to model
        /// </summary>
        /// <param name="dataset"></param>
        /// <param name="qualifiedPartsList"></param>
        /// <param name="searchBy"></param>
        private static void MapReportDataSourceToModel(DataSet dataset, 
            ICollection<QualifiedPartsList> qualifiedPartsList, 
            string searchBy)
        {
            try
            {


                for (var i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    var list = new QualifiedPartsList();

                    list.PartNumber = dataset.Tables[0].Rows[i]["PartNumber"].ToString();
                    list.PartRevision = dataset.Tables[0].Rows[i]["Revision"] != null
                        ? dataset.Tables[0].Rows[i]["Revision"].ToString()
                        : null;
                    list.PartName = dataset.Tables[0].Rows[i]["PartName"] != null
                        ? dataset.Tables[0].Rows[i]["PartName"].ToString()
                        : null;
                    list.ToolDieSetNumber = dataset.Tables[0].Rows[i]["ToolDieSetNumber"] != null
                        ? dataset.Tables[0].Rows[i]["ToolDieSetNumber"].ToString()
                        : null;
                    list.QPL = (bool)dataset.Tables[0].Rows[i]["QPL"] == true? "Yes" : "No";
                    list.OpenPO = (bool) dataset.Tables[0].Rows[i]["OpenPO"] == true? "Yes": "No";
                    list.PartJurisdiction = dataset.Tables[0].Rows[i]["Part Juisdication"] != null
                        ? dataset.Tables[0].Rows[i]["Part Juisdication"].ToString()
                        : null;
                    list.PartClassification = dataset.Tables[0].Rows[i]["Part Classification"] != null
                        ? dataset.Tables[0].Rows[i]["Part Classification"].ToString()
                        : null;
                    list.SupplierCompanyName = dataset.Tables[0].Rows[i]["Supplier Company Name"].ToString();
                    list.SupplierCompanyCode = dataset.Tables[0].Rows[i]["Supplier Company Code"].ToString();
                    list.CTQ = (bool) dataset.Tables[0].Rows[i]["CTQ"]== true? "Yes" : "No";
                    list.QPLLastUpdatedBy = dataset.Tables[0].Rows[i]["UserName"].ToString();
                    list.QPLLastUpdatedDate = ((DateTime) dataset.Tables[0].Rows[i]["LastUpdatedDateUtc"])
                        .ToShortDateString();
                    

                    qualifiedPartsList.Add(list);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception occured while Mapping DataSet to response object", ex);
                throw ex;
            }
        }
    }
}