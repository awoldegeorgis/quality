﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using log4net;

namespace Quality.Web.Provider
{
    public class DbProvider : IProvider
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const String ReportStoredProcedure = "sp_QualifiedPartsList_New";

        public string GetConnection()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["dataBaseConnection"].ConnectionString;

            return connectionString;
        }

        public DataSet GetReportData()
        {
            try
            {
                var dataset = new DataSet();
                var con = new SqlConnection {ConnectionString = GetConnection()};
                con.Open();

                using (var command = new SqlCommand(ReportStoredProcedure, con)
                {
                    CommandType = CommandType.StoredProcedure
                })
                {
                    var adapt = new SqlDataAdapter
                    {
                        SelectCommand = command
                    };

                    adapt.Fill(dataset);
                }

                return dataset;
            }
            catch (Exception ex)
            {
                Log.Error("Error occured while reading data from Database", ex);
                throw ex;
            }
        }
    }
}