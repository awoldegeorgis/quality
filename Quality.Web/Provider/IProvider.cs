﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quality.Web.Provider
{
    public interface IProvider
    {
        string GetConnection();

        DataSet GetReportData();
    }
}
