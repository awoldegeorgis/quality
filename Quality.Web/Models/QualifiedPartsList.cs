﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Quality.Web.Models
{
    public class QualifiedPartsList
    {
        public string PartNumber { get; set; }
        public string PartRevision { get; set; }

        public string PartName { get; set; }

        public string ToolDieSetNumber { get; set; }

        public string QPL { get; set; }

        public string OpenPO { get; set; }

        public string PartJurisdiction { get; set; }

        public string PartClassification { get; set; }

        public string SupplierCompanyName { get; set; }

        public string SupplierCompanyCode { get; set; }

        public string CTQ { get; set; }

        public string QPLLastUpdatedBy { get; set; }

        public string QPLLastUpdatedDate { get; set; }
    }
}