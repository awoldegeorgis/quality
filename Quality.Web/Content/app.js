console.log("Hello world");
var ReportController = (function () {
    function ReportController($scope, $rootScope, ReportService, $q) {
        var _this = this;
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.ReportService = ReportService;
        this.$q = $q;
        //Default pag number
        this.pageNumber = 1;
        //Default server item per page value
        this.itemPerPage = 10;
        this.pageSize = null;
        $scope.searchValue = null;
        $scope.sizeValue = null;
        //Default value for show option
        $scope.showModel = { code: 2, name: 'First' };
        $scope.itemsPerPage = [10, 20, 40, 60];
        $scope.sizeValue = 10;
        $scope.showOption = [{ code: 1, name: 'All' }, { code: 2, name: 'First' }, { code: 3, name: 'Last' }];
        var getData = function (page, size) {
            ReportService.getDataFromApi(page, size).then(function (data) {
                $scope.data = data;
            }, function (error) {
                $scope.eroror = error;
            });
        };
        getData(1, null);
        $scope.getNextPage = function () {
            if ((_this.pageNumber + 1) > $scope.data.TotalNumberOfPages) {
                return;
            }
            _this.pageNumber = $scope.data.PageNumber + 1;
            getData(_this.pageNumber, null);
        };
        $scope.getPreviousPage = function () {
            _this.pageNumber = $scope.data.PageNumber - 1;
            if (_this.pageNumber == 0) {
                return;
            }
            getData(_this.pageNumber, null);
        };
        $scope.getFirstPage = function () {
            getData(1, null);
        };
        $scope.getLastPage = function () {
            var lastPage = $scope.data.TotalNumberOfPages;
            getData(lastPage, null);
        };
        $scope.itemPerPageChange = function (itemPerPage) {
            console.log(itemPerPage + ":" + itemPerPage);
        };
        $scope.showUpdate = function () {
            console.log($scope.showModel);
            var code = $scope.showModel.code;
            switch (code) {
                case 1:
                    _this.pageNumber = 1;
                    _this.pageSize = $scope.data.TotalNumberOfRecords;
                    getData(_this.pageNumber, _this.pageSize);
                    break;
                case 2:
                    _this.pageNumber = 1;
                    getData(_this.pageNumber, null);
                    break;
                case 3:
                    _this.pageNumber = $scope.data.TotalNumberOfPages;
                    getData(_this.pageNumber, null);
                    break;
            }
        };
        $scope.search = function () {
            console.log($scope.searchValue);
        };
        $scope.pageSizeChange = function () {
            getData(1, $scope.sizeValue);
        };
    }
    ReportController.$inject = ["$scope", "$rootScope", "ReportService", "$q"];
    return ReportController;
}());
var ReportService = (function () {
    function ReportService($http, $q) {
        this.httpService = $http;
        this.qService = $q;
        this.baseUrl = "/api/QualifiedParts/GetReport";
    }
    ReportService.prototype.getDataFromApi = function (pageNumber, pageSize) {
        var pageQuery = "?page=" + pageNumber;
        var sizeQuery = "&PageSize=" + pageSize;
        pageQuery = pageSize == null ? pageQuery : pageQuery + sizeQuery;
        var deferred = this.qService.defer();
        this.httpService.get(this.baseUrl + pageQuery).then(function (res) {
            deferred.resolve(res.data);
        }).catch(function (res) {
            deferred.reject(res);
        });
        return deferred.promise;
    };
    ReportService.$inject = ["$http", "$q"];
    return ReportService;
}());
angular.module("Report", [])
    .controller("ReportController", ReportController)
    .service("ReportService", ReportService);
//# sourceMappingURL=app.js.map