﻿declare var angular: any;
declare var ng: any;

console.log("Hello world");



class ReportController {

    static $inject = ["$scope", "$rootScope","ReportService", "$q"];

    private q: any;
    private pageNumber: number;

    private itemPerPage: number; 

    private pageSize: number;
    
	constructor(
        private $scope,
        private $rootScope,
        private ReportService,
        private $q
    ) {

        //Default pag number
        this.pageNumber = 1;

        //Default server item per page value
        this.itemPerPage = 10;

        this.pageSize = null;

        $scope.searchValue = null;

	    $scope.sizeValue = null;

        //Default value for show option
        $scope.showModel = { code: 2, name: 'First' };

        $scope.itemsPerPage = [10, 20, 40, 60];

	    $scope.sizeValue = 10;
        
	    $scope.showOption = [{ code: 1, name: 'All' }, { code: 2, name: 'First' }, { code: 3, name: 'Last' }];

	    var getData = (page: number, size: number) => {
            ReportService.getDataFromApi(page, size).then(data => {
	                $scope.data = data;
	            },
	            error => {
	                $scope.eroror = error;
	            });
        }

	    getData(1, null);

        $scope.getNextPage = () => {
            
            if((this.pageNumber + 1) > $scope.data.TotalNumberOfPages) {
                return;
            }

            this.pageNumber = $scope.data.PageNumber + 1;

            getData(this.pageNumber, null);
	    }

        $scope.getPreviousPage = () => {
            this.pageNumber = $scope.data.PageNumber - 1;

            if (this.pageNumber == 0) {
                return;
            }

            getData(this.pageNumber, null);
        }
        
        $scope.getFirstPage = () => {
            getData(1, null);
        }

	    $scope.getLastPage = () => {
            var lastPage: number = $scope.data.TotalNumberOfPages;
            getData(lastPage, null);

        }  

        $scope.itemPerPageChange = (itemPerPage: number) => {
            console.log(itemPerPage + ":" + itemPerPage);
        }

        $scope.showUpdate = () => {
            console.log($scope.showModel);

            var code = $scope.showModel.code;

            switch (code) {
                case 1:
                    this.pageNumber = 1;
                    this.pageSize = $scope.data.TotalNumberOfRecords;
                    getData(this.pageNumber, this.pageSize);
                    break;
                case 2:
                    this.pageNumber = 1;
                    getData(this.pageNumber, null);
                    break;
                case 3:
                    this.pageNumber = $scope.data.TotalNumberOfPages;
                    getData(this.pageNumber,  null);
                    break;
            }
        }

        $scope.search = () => {
            console.log($scope.searchValue);
        }

        $scope.pageSizeChange = ()=> {            
            getData(1, $scope.sizeValue);
        }
	}    
}

class ReportService {

    static $inject = ["$http", "$q"];

    private httpService: any;
    private qService: any;
    private  baseUrl : string;

    constructor($http, $q) {
        this.httpService = $http;
        this.qService = $q;
        this.baseUrl = "/api/QualifiedParts/GetReport";
    }
        
        public getDataFromApi(pageNumber: Number, pageSize: Number): number {

            var pageQuery = "?page=" + pageNumber;
            var sizeQuery = "&PageSize=" + pageSize;

            pageQuery = pageSize == null ? pageQuery : pageQuery + sizeQuery;
 
            var deferred = this.qService.defer();

            this.httpService.get(this.baseUrl + pageQuery).then(res => {
                deferred.resolve(res.data);
            }).catch(res => {
                deferred.reject(res);
            });

            return deferred.promise;
        }
}

angular.module("Report", [])
    .controller("ReportController", ReportController)
    .service("ReportService", ReportService);